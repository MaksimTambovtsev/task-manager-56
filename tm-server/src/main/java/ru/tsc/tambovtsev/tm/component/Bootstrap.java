package ru.tsc.tambovtsev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.api.service.*;
import ru.tsc.tambovtsev.tm.api.service.dto.*;
import ru.tsc.tambovtsev.tm.endpoint.*;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.listener.EntityListener;
import ru.tsc.tambovtsev.tm.listener.EntityLogListener;
import ru.tsc.tambovtsev.tm.util.HashUtil;
import ru.tsc.tambovtsev.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ITaskService taskServiceDTO;

    @Getter
    @NotNull
    @Autowired
    private IProjectService projectServiceDTO;

    @Getter
    @NotNull
    @Autowired(required=false)
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IUserService userServiceDTO;

    @Getter
    @NotNull
    @Autowired
    private IAuthService authService;

    @Getter
    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    @NotNull
    private Backup backup = new Backup(this);

    @SneakyThrows
    public void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    public void run() {
        initEndpoints();
        initPID();
        initJMS();
        initUsers();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
        backup.stop();
    }

    @SneakyThrows
    private void initUsers() {
        @Nullable UserDTO user = userServiceDTO.findByLogin("test");
        if (user == null) {
            user = new UserDTO();
            user.setEmail("test@test.ru");
            user.setLogin("test");
            user.setPasswordHash(HashUtil.salt(propertyService, "test"));
            userServiceDTO.create(user);
        }
        user = userServiceDTO.findByLogin("admin");
        if (user == null) {
            user = new UserDTO();
            user.setRole(Role.ADMIN);
            user.setLogin("admin");
            user.setPasswordHash(HashUtil.salt(propertyService, "admin"));
            userServiceDTO.create(user);
        }
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = "localhost";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?WSDL";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void initJMS() {
        BasicConfigurator.configure();
        EntityListener.setConsumer(new EntityLogListener());
    }

}