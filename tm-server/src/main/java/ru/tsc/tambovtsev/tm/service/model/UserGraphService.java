package ru.tsc.tambovtsev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.repository.model.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.repository.model.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;
import ru.tsc.tambovtsev.tm.api.service.model.IUserService;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.repository.dto.UserRepository;
import ru.tsc.tambovtsev.tm.repository.model.UserGraphRepository;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class UserGraphService extends AbstractGraphService<User, IUserRepository> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Nullable
    @Autowired
    private IProjectRepository projectRepository;

    @Nullable
    @Autowired
    private ITaskRepository taskRepository;

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserRepository repository = new UserGraphRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final User user = repository.findByLogin(login);
            return user;
        }
        finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @NotNull final IUserRepository repository = new UserGraphRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final User user = repository.findByEmail(email);
            return user;
        }
        finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserRepository repository = new UserGraphRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final User user = findByLogin(login);
            if (user == null) return null;
            entityManager.getTransaction().begin();
            repository.removeCascade(user);
            entityManager.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User setPassword(
            @Nullable final String userId,
            @Nullable final String password
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        @NotNull final IUserRepository repository = new UserGraphRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final User user = repository.findById(userId);
            if (user == null) return null;
            final String hash = HashUtil.salt(propertyService, password);
            user.setPasswordHash(hash);
            entityManager.getTransaction().begin();
            repository.updateUser(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return null;
    }

    @Override
    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserRepository repository = new UserGraphRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final User user = findByLogin(login);
            entityManager.getTransaction().begin();
            repository.lockUserById(user.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @NotNull final IUserRepository repository = new UserGraphRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final User user = findByLogin(login);
            entityManager.getTransaction().begin();
            repository.unlockUserById(user.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e){
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public IUserRepository getRepository() {
        return context.getBean(IUserRepository.class);
    }

    @Override
    public void addAll(@NotNull final Collection<User> models) {
        @NotNull final IUserRepository repository = new UserGraphRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<User> findAll() {
        @NotNull final IUserRepository repository = new UserGraphRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final List<User> result = repository.findAllUser();
            return result;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        @NotNull final ru.tsc.tambovtsev.tm.api.repository.dto.IUserRepository repository = new UserRepository();
        return repository.getEntityManager();
    }

    @Override
    @NotNull
    public Collection<User> set(@NotNull final Collection<User> models) {
        @NotNull final IUserRepository repository = new UserGraphRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clearUser();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return models;
    }

}
