package ru.tsc.tambovtsev.tm.configuration;

import lombok.NonNull;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.tsc.tambovtsev.tm.api.service.IDatabaseProperty;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.model.Task;
import ru.tsc.tambovtsev.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("ru.tsc.tambovtsev.tm")
public class ServerConfiguration {

    @Bean
    @NonNull
    public EntityManagerFactory entityManagerFactory (@NonNull final IDatabaseProperty databaseProperties) {
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, databaseProperties.getDatabaseDriver());
        settings.put(Environment.URL, databaseProperties.getDatabaseUrl());
        settings.put(Environment.USER, databaseProperties.getDatabaseUsername());
        settings.put(Environment.PASS, databaseProperties.getDatabasePassword());
        settings.put(Environment.DIALECT, databaseProperties.getSQLDialect());
        settings.put(Environment.HBM2DDL_AUTO, databaseProperties.getHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, databaseProperties.getShowSql());
        settings.put(Environment.FORMAT_SQL, databaseProperties.getFormatSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperties.getSecondLvlCash());
        settings.put(Environment.CACHE_REGION_FACTORY, databaseProperties.getFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, databaseProperties.getUseQueryCash());
        settings.put(Environment.USE_MINIMAL_PUTS, databaseProperties.getUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, databaseProperties.getRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperties.getConfigFilePath());
        @NotNull final StandardServiceRegistryBuilder registryBuilder =
                new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry serviceRegistry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(serviceRegistry);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager (@NotNull final EntityManagerFactory  entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}
