package ru.tsc.tambovtsev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IOwnerRepository<Project> {

    void create(@NotNull Project project);

    void updateById(@NotNull Project project);

    @Nullable
    Project findById(@Nullable String userId, @Nullable String id);

    void clear(@Nullable String userId);

    long getSize(@Nullable String userId);

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    List<Project> findAllProject();

    void removeByIdProject(@Nullable String userId, @Nullable String id);

    void removeById(@Nullable String id);

    void clearProject();

}