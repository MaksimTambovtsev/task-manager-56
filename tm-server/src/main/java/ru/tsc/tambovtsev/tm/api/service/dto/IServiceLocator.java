package ru.tsc.tambovtsev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.service.IDomainService;
import ru.tsc.tambovtsev.tm.api.service.ILoggerService;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;

public interface IServiceLocator {

    @NotNull
    ITaskService getTaskServiceDTO();

    @NotNull
    IProjectService getProjectServiceDTO();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IUserService getUserServiceDTO();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    IPropertyService getPropertyService();

}
