package ru.tsc.tambovtsev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UserLogoutResponse extends AbstractResultResponse {

    public UserLogoutResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
