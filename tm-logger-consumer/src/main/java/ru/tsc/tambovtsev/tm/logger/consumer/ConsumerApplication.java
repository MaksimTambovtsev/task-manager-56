package ru.tsc.tambovtsev.tm.logger.consumer;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.tambovtsev.tm.logger.consumer.bootstrap.BootstrapLogger;
import ru.tsc.tambovtsev.tm.logger.consumer.configuration.LoggerConfiguration;

public final class ConsumerApplication {

    @SneakyThrows
    public static void main(@NotNull final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final BootstrapLogger bootstrapLogger = context.getBean(BootstrapLogger.class);
        bootstrapLogger.run();
    }

}