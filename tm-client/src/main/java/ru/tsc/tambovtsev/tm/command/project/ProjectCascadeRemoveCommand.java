package ru.tsc.tambovtsev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

@Component
public final class ProjectCascadeRemoveCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-cascade-remove";

    @NotNull
    public static final String DESCRIPTION = "Project cascade remove.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[CASCADE REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        //getProjectTaskService().removeProjectById(userId, projectId);
    }

}
