package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    ITokenService getTokenService();

}
