package ru.tsc.tambovtsev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.dto.request.ProjectUpdateByIdRequest;
import ru.tsc.tambovtsev.tm.dto.request.TaskBindProjectRequest;
import ru.tsc.tambovtsev.tm.util.TerminalUtil;

@Component
public final class TaskBindToProjectCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "bind-task-to-project";

    @NotNull
    public static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    public static final String ARGUMENT = null;

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        getTaskEndpoint().bindProjectTask(new TaskBindProjectRequest(userId, projectId, taskId));
    }

}
