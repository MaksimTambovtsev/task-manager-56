package ru.tsc.tambovtsev.tm.command.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.dto.request.DataXmlSaveFasterXmlRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;

@Component
public class DomainSaveXMLFasterXMLCommand extends AbstractDomainCommand {

    @NotNull
    private final static String NAME = "save-xml-fasterxml";

    @NotNull
    private final static String DESCRIPTION = "Save projects, tasks and users in xml fasterxml";

    @Nullable
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        getDomainEndpoint().saveDataXmlFasterXml(new DataXmlSaveFasterXmlRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
